﻿using System;
using cAlgo.API;
using System.Collections.Generic;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class TheGrid : Robot
    {
        [Parameter(DefaultValue = 5, MinValue = 5)]
        public int Period { get; set; }

        [Parameter(DefaultValue = 10, MinValue = 1)]
        public int MinSizeChannel { get; set; }

        [Parameter(DefaultValue = 12, MinValue = 5)]
        public int MaxSizeChannel { get; set; }

        [Parameter(DefaultValue = 7, MinValue = 5)]
        public int QtdChannel { get; set; }

        [Output("Up Fractal", LineColor = "#e74c3c", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries UpFractal { get; set; }

        [Output("Down Fractal", LineColor = "#3498db", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries DownFractal { get; set; }

        [Output("Channel", LineColor = "#8e44ad", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries ChannelPoint { get; set; }

        public double currentChannel;

        public bool isCanal;

        public List<ChartLine> ChartLineValues = new List<ChartLine>();

        Target target = new Target();

        public enum ModeType
        {
            MODE_UPPER,
            MODE_LOWER
        }

        public class ChartLine
        {
            public string name;
            public double value;
        }

        public class Target
        {
            public ChartLine upperTarget;
            public ChartLine lowerTarget;
        }

        protected override void OnStart()
        {
            TheGridExecute();
        }

        private void TheGridExecute()
        {
            int currentBars = Bars - 1;
            int highCandle = 0;
            isCanal = false;
            bool isFractal = false;

            for (int currentBar = currentBars; currentBar > 1; currentBar--)
            {
                int resistence = iFractal(currentBar, ModeType.MODE_UPPER);

                if (resistence > 0 && isFractal == false)
                {

                    //UpFractal[resistence] = MarketSeries.High[resistence];
                    currentChannel = MarketSeries.High[resistence];
                    highCandle = currentBar;
                    isFractal = true;
                }

                int support = iFractal(currentBar, ModeType.MODE_LOWER);

                if (support > 0 && isFractal == true)
                {
                    isFractal = false;
                    //DownFractal[support] = MarketSeries.Low[support];
                    currentChannel = Math.Abs((currentChannel - MarketSeries.Low[support]) / Symbol.PipValue);

                    if (currentChannel >= MinSizeChannel && currentChannel <= MaxSizeChannel && isCanal == false)
                    {
                        isCanal = true;
                        Print("TAMANHO DO CANAL ATUAL ->", currentChannel);

                        drawChartLine("RESISTENCE", MarketSeries.High[highCandle], Color.Red, 2);
                        drawChartLine("SUPPORT", MarketSeries.Low[support], Color.Blue, 2);

                        drawChannelsUpper("TP/SL_TOP_", MarketSeries.High[highCandle]);
                        drawChannelsLower("TP/SL_BOTTOM_", MarketSeries.Low[support]);

                        //ChannelPoint[highCandle] = MarketSeries.High[highCandle];
                        //ChannelPoint[support] = MarketSeries.Low[support];

                        getChartLines();
                        getTargets();
                        break;

                    }
                }
            }
        }

        private int iFractal(int index, ModeType modeType)
        {
            int period = Period % 2 == 0 ? Period - 1 : Period;
            int middleIndex = index - period / 2;
            double middleValue = modeType == ModeType.MODE_UPPER ? MarketSeries.High[middleIndex] : MarketSeries.Low[middleIndex];

            bool down = true;
            bool up = true;

            if (modeType == ModeType.MODE_UPPER)
            {
                for (int i = 0; i < period; i++)
                {
                    if (middleValue < MarketSeries.High[index - i])
                    {
                        up = false;
                        break;
                    }
                }
                if (up)
                {
                    return middleIndex;
                }
                else
                {
                    return 0;
                }

            }

            if (modeType == ModeType.MODE_LOWER)
            {
                for (int i = 0; i < period; i++)
                {
                    if (middleValue > MarketSeries.Low[index - i])
                    {
                        down = false;
                        break;
                    }
                }
                if (down)
                {
                    return middleIndex;
                }
                else
                {
                    return 0;
                }
            }

            return 0;

        }

        private void drawChartLine(string name, double value, Color color, int thickness)
        {
            Chart.DrawHorizontalLine(name, value, color, thickness);
        }

        private int Bars
        {
            get { return MarketSeries.Close.Count; }
        }

        private void drawChannelsUpper(string name, double price)
        {
            for (int i = 0; i < QtdChannel; i++)
            {
                price = price + (currentChannel * Symbol.PipValue);
                drawChartLine(name + i, price, Color.Green, 2);
            }
        }

        private void drawChannelsLower(string name, double price)
        {
            for (int i = 0; i < QtdChannel; i++)
            {
                price = price - (currentChannel * Symbol.PipValue);
                drawChartLine(name + i, price, Color.Green, 2);
            }
        }

        private void getChartLines()
        {

            var lines = Chart.FindAllObjects<ChartHorizontalLine>();

            foreach (var line in lines)
            {
                ChartLine chartLine = new ChartLine();
                chartLine.name = line.Name;
                chartLine.value = line.Y;
                ChartLineValues.Add(chartLine);
            }

            ChartLineValues.Sort((x, y) => y.value.CompareTo(x.value));

        }

        private void getTargets()
        {


            for (int i = ChartLineValues.Count - 1; i > 0; i--)
            {
                if (Symbol.Bid < ChartLineValues[i].value)
                {
                    target.upperTarget = ChartLineValues[i];
                    break;
                }
            }

            for (int i = 0; i < ChartLineValues.Count; i++)
            {
                if (Symbol.Bid > ChartLineValues[i].value)
                {
                    target.lowerTarget = ChartLineValues[i];
                    break;
                }
            }

        }


        protected override void OnTick()
        {


            if (Symbol.Bid > target.upperTarget.value && Positions.Count == 0)
            {
                Print("<--COMPRA-->");
                int volume = 10000;
                double stopLoss = getStopLoss(TradeType.Buy);
                double takeProfit = getTakeProfit(TradeType.Buy);
                TradeResult tradeResult = ExecuteMarketOrder(TradeType.Buy, Symbol.Name, volume, "label", stopLoss, takeProfit);
                Print(tradeResult.ToString());
            }

            if (Symbol.Bid < target.lowerTarget.value && Positions.Count == 0)
            {
                Print("<--VENDA-->");
                int volume = 10000;
                double stopLoss = getStopLoss(TradeType.Buy);
                double takeProfit = getTakeProfit(TradeType.Buy);
                TradeResult tradeResult = ExecuteMarketOrder(TradeType.Sell, Symbol.Name, volume, "label", stopLoss, takeProfit);
                Print(tradeResult.ToString());
            }

            getTargets();
        }

        public double getStopLoss(TradeType tradeType)
        {
            double stopLoss = 0;
            stopLoss = currentChannel * 2;
            return stopLoss;
        }

        public double getTakeProfit(TradeType tradeType)
        {
            double takeProfit = 0;

            takeProfit = currentChannel * 2;
            return takeProfit;
        }

        protected override void OnStop()
        {
            // Put your deinitialization logic here
        }
    }
}
